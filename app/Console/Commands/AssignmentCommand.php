<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class AssignmentCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assignment:command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
               // globally taken static string as $request as shown below
                $request = "hello world";
                // Alternative uppercase and lowercase functionality
                $letter_count = 0;
                $result = '';
                for ($i=0; $i<strlen($request); $i++) {
                    if (!preg_match('![a-zA-Z]!', $request[$i])) {
                        $result .= $request[$i];
                    } else if ($letter_count++ & 1) {
                        $result .= strtoupper($request[$i]);
                    } else {
                        $result .= $request[$i];
                    }
                }
                // csv download functionality
                $array_lst = str_split($request);
                $list = implode(',', $array_lst);
                $list = array([$list]); 
                $fp = fopen('hello-iprice.csv', 'w'); 
                    foreach ($list as $fields) { 
                        fputcsv($fp, $fields); 
                    } 
                        fclose($fp);
                // total response
                $result = strtoupper($request).PHP_EOL.$result.PHP_EOL."CSV created".PHP_EOL;
          
                print $result;
    }
}